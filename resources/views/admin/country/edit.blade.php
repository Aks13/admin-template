@extends('admin.layouts.admin-lte')

@section('title', __('general.countries'))
@section('content-title', __('general.countries'))


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('countries.index') }}"><i class="fa fa-building"></i> {{ __('general.countries') }}</a></li>
        <li class="active"><i class="fa fa-building"></i> {{ __('general.edit') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($country, ['route' => ['countries.update', $country],'method' => 'put','files' => true]) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.country_edit') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}

                        <a href="{{ route('countries.index') }}" class="btn btn-default"><i class="fa fa-share"></i></a>
                    </div>
                </div>
                <div class="box-body">
                    @include('admin.country.partials.form')
                    {!! Form::hidden('modified_by', Auth::id()) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
