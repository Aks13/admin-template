@extends('admin.layouts.admin-lte')

@section('title', __('general.category'))
@section('content-title', __('general.category'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('category.index') }}"><i class="fa fa-building"></i> {{ __('general.category') }}</a></li>
        <li class="active"><i class="fa fa-building"></i> {{ __('general.new') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open(['route' => 'category.create-packet','method' => 'post','files' => true]) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.category_creating') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}

                        <a href="{{ route('category.index') }}" class="btn btn-default"><i class="fa fa-share"></i></a>
                    </div>
                </div>
                <div class="box-body">

                    <div class="col-sm-6">

                        <div class="form-group {{ $errors->has('parent_id') ? 'has-error' : ''}}">
                            {!! Form::label('parent_id', __('general.category_parent')) !!}
                            {!! Form::select(
                            'parent_id',
                             $categories_global,
                             $category->parent_id ?? null,
                             ['class' => 'form-control']
                             ) !!}
                            {!! $errors->first('parent_id', '<p class="help-block">:message</p>') !!}
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="form-group">
                            {!! Form::label('explode_symbol', __('general.explode_symbol')) !!}
                            {!! Form::text('explode_symbol', '@',['class' => 'form-control','placeholder' => __('general.explode_symbol'),'size' => 255]) !!}
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="form-group">
                            {!! Form::label('category_packet', __('general.category_packet')) !!}
                            {!! Form::file('category_packet',['accept' => '.txt','class' => 'form-control','placeholder' => __('general.category_packet')]) !!}
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="form-group">
                            {!! Form::label('parent_id', __('general.packet')) !!}
                            {!! Form::textarea('packet',null,['class' => 'form-control','placeholder' => __('general.packet')]) !!}
                        </div>

                    </div>
                    {!! Form::hidden('created_by', Auth::id()) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
