<div class="row">
    <div class="col-sm-6">
        <label for="category[]">Select categories</label>
        <select class="form-control select2 select2-hidden-accessible" multiple="" name="category[]" data-placeholder="Select categories" style="width: 100%;" tabindex="-1" aria-hidden="true">
            @foreach(\App\Models\Category::withoutParents() as $category)
                <option value="{{$category->id}}">{{$category->title}}</option>
                @if(!empty($category->children) && count($category->children)>0)
                    <optgroup label="{{$category->title}}">
                        @foreach($category->children as $child)
                            <option value="{{$child->id}}">{{$child->title}}</option>
                        @endforeach
                    </optgroup>
                    <option disabled>_________</option>
                @endif
            @endforeach
        </select>

    </div>
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
            {!! Form::label('images[]', __('general.images')) !!}
            {!! Form::file('images[]', ['accept'=>'image/*','class' => 'form-control','placeholder' => __('general.title'), 'multiple'=>'multiple']) !!}
            {!! $errors->first('images[]', '<p class="help-block">:message</p>') !!}
        </div>

    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function(){
        $('.select2').select2();
    });
</script>
