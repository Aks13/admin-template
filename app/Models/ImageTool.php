<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ImageTool extends Model
{
    public static function resize($filename, $width, $height) {
        $DIR_IMAGE = $_SERVER['DOCUMENT_ROOT'].'/storage/images/';

        if (!is_file($DIR_IMAGE . $filename)) {
            return null;
        }


        $request = request();
        if($width == 0 && $height == 0){
            return $request->server->get('REQUEST_SCHEME') .'://'. $request->server->get('HTTP_HOST') . '/storage/images/' . $filename;
        }

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $image_old = $filename;
        $image_new = 'cache/' . mb_substr($filename, 0, mb_strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;

        if (!is_file($DIR_IMAGE . $image_new) || (filemtime($DIR_IMAGE . $image_old) > filemtime($DIR_IMAGE . $image_new))) {
            list($width_orig, $height_orig, $image_type) = getimagesize($DIR_IMAGE . $image_old);

            if (!in_array($image_type, array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF))) {
                return $DIR_IMAGE . $image_old;
            }

            $path = '';

            $directories = explode('/', dirname($image_new));

            foreach ($directories as $directory) {
                $path = $path . '/' . $directory;

                if (!is_dir($DIR_IMAGE . $path)) {
                    @mkdir($DIR_IMAGE . $path, 0777);
                }
            }

            if ($width_orig != $width || $height_orig != $height) {
                $image = new Image($DIR_IMAGE . $image_old);
                $image->resize($width, $height);
                $image->create($DIR_IMAGE . $image_new);
            } else {
                copy($DIR_IMAGE . $image_old, $DIR_IMAGE . $image_new);
            }
        }

        return $request->server->get('REQUEST_SCHEME') .'://'. $request->server->get('HTTP_HOST') . '/storage/images/' . $image_new;
    }

    public static function resizeFromStorage($filename, $width, $height = null) {
        $DIR_IMAGE = storage_path('/storage/app/');

        if (!is_file($filename)) {
            return null;
        }
        $size = getimagesize($filename);
        $c_width = $size[0];
        $c_height = $size[1];
        $image = new Image($filename);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $image_new = 'public/images/cache/catalog/' . hash_file('sha256',$filename) . '-' . $width . '.' . $extension;
        if($c_width > $width)
        {
            if (!$height) { $height = (int)$c_height*($width/$c_width); }
//            dd((int)$height);
            $image->resize((int)$width, (int)$height);
            if($image->getMime() == 'image/gif')
            {
                $image->createGif($image_new,(int)$width, (int)$height);
            }
            else
            {
                $image->createImg($image_new,(int)$width, (int)$height);
            }
        }
        else
        {
            Storage::put( str_replace('/',"\\",$image_new ) , file_get_contents($filename));
        }

        return $image_new;
    }
}
