@extends('admin.layouts.admin-lte')

@section('title', __('general.images'))
@section('content-title', __('general.images'))

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.search') }}</h3>
                </div>
                <div class="box-body">
                    @include('admin.gsearch.partials.form')
                </div>
            </div>
        </div>
    </div>
    <script>
        function ids(){
            var ids = [];
            $('input#id').each(function () {
                $(this).prop('checked') ? ids.push($(this).val()) : null;
            });
            $('input#ids-to-delete').val(JSON.stringify(ids));
        }
    </script>
@endsection

