<!-- Tasks Menu -->
<li class="dropdown tasks-menu">
    <!-- Menu Toggle Button -->
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
    </a>
    <ul class="dropdown-menu">
        {{--<li class="header">Select language from list</li>--}}
        <li class="header">{{__('general.language') }}</li>
        <ul class="menu">
            <li><a href="{{ route('lang',['locale'=>'en']) }}">{{__('general.gb') }}</a></li>
            <li><a href="{{ route('lang',['locale'=>'ru']) }}">{{__('general.ru') }}</a></li>
        </ul>
    </ul>
</li>
