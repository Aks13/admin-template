<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ImageThumbnail;
use App\Models\Category;
use App\Models\ImageCategory;
use App\Models\ImageDB;
use App\Models\ImageTool;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use JanDrda\LaravelGoogleCustomSearchEngine\LaravelGoogleCustomSearchEngine;


/**
 * Are you idiot or what? Why did you extend AdminControlers from Controller?
 * Really?
 * You can look to another controllers and you can see whole
 * controler in admin groups extends from AdminController
 */
class GSearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasPermission:admin');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories_global = Category::whereIn('id',Category::GLOBAL_CATEGORIES)->get();
        return view('admin.gsearch.index', [
            'categories_global'=>$categories_global,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.gsearch.create', [

        ]);
    }

    public function search(Request $request)
    {
        $data = $request->all();

        if(empty($data['search-query']))
        {
            return redirect()
                ->route('google-search.index')
                ->with( 'message.level', 'error' )
                ->with( 'message.content', 'Empty search query!' );
        }
        if(isset($data['images']) && !empty($data['images']))
        {
            isset($data['category']) ? null : $data['category'] = Category::GLOBAL_CATEGORIES;
            $categories = Category::whereIn('id',$data['category'])->get();
            foreach ($data['images'] as $image)
            {
                $file = file_get_contents($image['src']);
                $file_info = pathinfo($image['src']);
                $path = 'public/images/'. time(). $file_info['filename'].'.'.$file_info['extension'];
                Storage::put( str_replace('/',"\\",$path ) , $file);

                $thumbnail = $path; //TODO make resize

                $imagedb = ImageDB::create([
                    'title' => $image['name'],
                    'path' => $path,
                    'thumbnail' => $thumbnail,
                    'created_by' => $data['created_by'],
                ]);
                $type = strtolower($file_info['extension']);
                if ($type == 'gif')
                {
                    $imagedb->type = 'gif';
                }
                $imagedb->save();

//                (new ImageThumbnail($imagedb))->dispatch();

                ImageThumbnail::dispatch($imagedb);

                foreach ($categories as $category)
                {
                    $image_category = ImageCategory::create([
                        'image_id' => $imagedb->id,
                        'category_id' => $category->id,
                    ]);
                    $image_category->save();
                }
            }
        }

        $categories_global = Category::whereIn('id',Category::GLOBAL_CATEGORIES)->get();

        $parameters = array(
            'start' => $data['search-start'] ?? 1,
            'num' => (int)$data['search-count'] ?? 10,
            'searchType' => "image",
            'filter' => "1",
            'fileType' => $data['search-mime']??'*',
        );
        $data['imgType'] ? $parameters['imgType'] = $data['imgType'] : null;
        $data['imgSize'] ? $parameters['imgSize'] = $data['imgSize'] : null;
        $max_results_count = 100;

        $fulltext = new LaravelGoogleCustomSearchEngine();

        $results = $fulltext->getResults($data['search-query'], $parameters);
        $info = $fulltext->getSearchInformation();
        $info->totalResults < $max_results_count ? $max_results_count = $info->totalResults : null;

        $pages_count = $max_results_count/$parameters['num'];

        $prev_page = [];
        $parameters['start'] > 1 ? $prev_page = [
            'val' => ($parameters['start'] - $parameters['num']) > 0 ? ($parameters['start'] - $parameters['num']) : 1,
            'name' => 'Previous',
            'class' => 'previous']
            : $prev_page = [
            'val' => null,
            'name' => 'Previous',
            'class' => 'previous disabled'];

        $next_page = [];
        ($parameters['start'] + $parameters['num']) <= $max_results_count ? $next_page = [
            'val' => ($parameters['start'] + $parameters['num']) ?? null,
            'name' => 'Next',
            'class' => 'next']
            : $prev_page = [
            'val' => null,
            'name' => 'Next',
            'class' => 'next disabled'];

        $pagination = [
            $prev_page,
        ];
//        dd($next_page);

        $i=0;
        $val = 1;
        while($i < $pages_count)
        {
            $i++;
            $class = '';
            if($val==$parameters['start'])
            {
                $class = 'active';
            }
            array_push($pagination,[
                'name' => $i,
                'val' => $val,
                'class' => $class,
            ]);
            $val += $parameters['num'];
        }
        array_push($pagination,$next_page);

        $category_selected = Category::GLOBAL_CATEGORIES;
        switch($data['search-mime'])
        {
            case 'gif' : {
                $category_selected = Category::where('title', 'GIF')->pluck('id');
                $category_selected = (array)$category_selected[0];
//                dd($category_selected);
                break;
            }
            case 'jpg' :
            case 'png' : {
                $category_selected = Category::where('title', 'PIC')->pluck('id');
                $category_selected = (array)$category_selected[0];
//                dd($category_selected);
                break;
            }
        }
        if(isset($data['category']) && !empty($data['category']))
        {
            $category_selected = $data['category'];
        }
//        dd($pagination);
        return view('admin.gsearch.search', [
            'data' => $data,
            'categories_global'=>$categories_global,
            'results' => $results,
            'info' => $info,
            'parameters' => $parameters,
            'pagination' => $pagination,
            'category_selected' => $category_selected,
            'i' => 0,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $category = Category::where('title','=',$data['category'])->first();
        foreach ($data['images'] as $image)
        {
            $file = file_get_contents($image['src']);
            $file_info = pathinfo($image['src']);
            $path = 'public/'.$data['category'].'/'. time(). $file_info['filename'].'.'.$file_info['extension'];
            Storage::put( str_replace('/',"\\",$path ) , $file);

            $thumbnail = $path; //TODO make resize

            $imagedb = ImageDB::create([
                'title' => $image['name'],
                'path' => $path,
                'thumbnail' => $thumbnail,
                'created_by' => $data['created_by'],
            ]);
            $type = strtolower($file_info['extension']);
            if ($type == 'gif')
            {
                $image->type = 'gif';
            }
            $imagedb->save();

            ImageThumbnail::dispatch($imagedb);

            $image_category = ImageCategory::create([
                'image_id' => $imagedb->id,
                'category_id' => $category->id,
            ]);
            $image_category->save();
        }
        return redirect()
            ->route('google-search.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
