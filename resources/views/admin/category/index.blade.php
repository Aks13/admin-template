@extends('admin.layouts.admin-lte')

@section('title', __('general.category'))
@section('content-title', __('general.category'))

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.categories_list') }}</h3>
                    <div class="pull-right">
                        <a href="{{ route('category.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                        <a href="{{ route('category.new-packet') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.category_packet') }}
                        </a>
                        <div class="pull-right mr-2">
                            <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('category.multi-delete') }}" method="post">
                                <input type="hidden" name="_method" value="delete" />
                                <input id="ids-to-delete" type="hidden" name="ids" value="" />
                                {{csrf_field()}}
                                <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i> Delete selected</button>
                            </form>
                        </div>

                        {{--<a href="" id="categories_file" data-toggle="image" class="btn btn-sm btn-success add_new_product">--}}
                            {{--<img src="" alt="" title="" data-placeholder="test">--}}
                        {{--</a>--}}
                        {{--<input type="hidden" name="categories_file" value="" id="categories_file">--}}
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th></th>
                                <th>{{ __('general.id') }}</th>
                                <th>{{ __('general.name') }}</th>
                                <th>{{ __('general.category_parent') }}</th>
                                {{--<th>{{ __('general.zip_code') }}</th>--}}
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        {{--{{dd($categories)}}--}}
                        @forelse($categories as $category)
                            <tr>
                                <td>
                                    @if(!in_array($category->id,\App\Models\Category::GLOBAL_CATEGORIES))
                                        <input type="checkbox" id="id" onclick="ids();" value="{{$category->id}}">
                                    @endif
                                </td>
                                <td>{{ $category->id }}</td>
                                <td>{{ $category->title }}</td>
                                <td>{{ $category->parent ? $category->parent->title : null }}</td>
{{--                                <td>{{ $category->parent->title ?? null }}</td>--}}
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('category.edit', $category) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    @if(!in_array($category->id,\App\Models\Category::GLOBAL_CATEGORIES))
                                        <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('category.destroy', $category) }}" method="post">
                                            <input type="hidden" name="_method" value="delete" />
                                            {{csrf_field()}}
                                            <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$categories->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <script>
        function ids(){
            var ids = [];
            $('input#id').each(function () {
                $(this).prop('checked') ? ids.push($(this).val()) : null;
            });
            $('input#ids-to-delete').val(JSON.stringify(ids));
        }
    </script>
@endsection

