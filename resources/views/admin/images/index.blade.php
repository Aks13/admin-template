@extends('admin.layouts.admin-lte')

@section('title', __('general.images'))
@section('content-title', __('general.images'))

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <form action="{{route('images.delete')}}" method="POST">
            <div class="box box-primary">
                <div class="box-header with-border row-no-gutters">
                    <div class="pull-left col row-no-gutters">
                        {!! \App\Models\Category::adminnavigation() !!}
                        {{--<form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('category.multi-delete') }}" method="post">--}}
                            {{--<input type="hidden" name="_method" value="delete" />--}}
                            {{--<input id="ids-to-delete" type="hidden" name="ids" value="" />--}}
                            {{--{{csrf_field()}}--}}
                            {{--<button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>--}}
                        {{--</form>--}}
                    </div>
                    <div class="pull-right col">
                        <button class="btn btn-danger btn-sm" type="submit" onclick="idstodelete();"><i class="fa fa-trash"></i> Delete selected</button>
                        <input hidden id="ids-to-delete" name="ids">
                        <a href="{{ route('images.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                        {{--<a href="{{ route('image.new-packet') }}" class="btn btn-sm btn-success add_new_product">--}}
                            {{--{{ __('general.category_packet') }}--}}
                        {{--</a>--}}

                        {{--<a href="" id="categories_file" data-toggle="image" class="btn btn-sm btn-success add_new_product">--}}
                            {{--<img src="" alt="" title="" data-placeholder="test">--}}
                        {{--</a>--}}
                        {{--<input type="hidden" name="categories_file" value="" id="categories_file">--}}
                    </div>
                </div>
                <div class="box-body">

                    <div class="row-no-gutters">
                            <ul class="grid">
                                @foreach($images as $image)

                                    <li class="grid-item">
                                        <div class="grid-item-wrapper">

                                            <span class="mailbox-attachment-icon has-img">
                                                <a href="/{{ $image->src() }}" data-fancybox>
                                                    <img src="/{{ $image->src() }}" alt="Attachment">
                                                </a>
                                            </span>

                                            <div class="mailbox-attachment-info">
                                                <p class="mailbox-attachment-name">
                                                    <input type="checkbox" id="id" value="{{ $image->id }}" onclick="idstodelete();">
                                                    <i class="fa fa-camera"></i>
                                                    {{ $image->title }}
                                                </p>

                                                <span class="mailbox-attachment-size flex-reverse-custom">
                                                    <a href="/{{ $image->src() }}" class="btn btn-primary btn-xs pull-right">
                                                        <i class="fa fa-cloud-download"></i>
                                                    </a>

                                                    @foreach($image->categories as $category)
                                                        <small>{{ $category->title }}</small>
                                                    @endforeach

                                                    @include('admin.images.partials.formdelete')
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach

                            </ul>
                    </div>

                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$images->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
            </form>
        </div>
    </div>
    <script>
        function idstodelete(){
            var ids = [];
            $('input#id').each(function () {
                $(this).prop('checked') ? ids.push($(this).val()) : null;
            });
            console.log(JSON.stringify(ids));
            $('input#ids-to-delete').val(JSON.stringify(ids));
        }
    </script>
@endsection

