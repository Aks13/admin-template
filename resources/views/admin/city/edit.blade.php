@extends('admin.layouts.admin-lte')

@section('title', __('general.cities'))
@section('content-title', __('general.cities'))


@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('general.dashboard') }}</a></li>
        <li><a href="{{ route('cities.index') }}"><i class="fa fa-building"></i> {{ __('general.cities') }}</a></li>
        <li class="active"><i class="fa fa-building"></i> {{ __('general.edit') }}</li>
    </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            {!! Form::model($city, ['route' => ['cities.update', $city],'method' => 'put','files' => true]) !!}
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.city_edit') }}</h3>
                    <div class="pull-right">
                        {!! Form::submit(__('general.save'), ['class' => 'btn btn-success']) !!}

                        <a href="{{ route('cities.index') }}" class="btn btn-default"><i class="fa fa-share"></i></a>
                    </div>
                </div>
                <div class="box-body">
                    @include('admin.city.partials.form')
                    {!! Form::hidden('modified_by', Auth::id()) !!}
                </div>

            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
