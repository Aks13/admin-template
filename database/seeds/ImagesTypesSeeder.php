<?php

use Illuminate\Database\Seeder;

class ImagesTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (\App\Models\ImageDB::all() as $image) {
            $info = pathinfo(storage_path('app/'.$image->path));
            $type = strtolower($info['extension']);
            if ($type == 'gif')
            {
                $image->type = 'gif';
                $image->save();
            }
        }
    }
}
