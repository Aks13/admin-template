<?php

use Illuminate\Database\Seeder;

class GeneralSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\GeneralSetting::create(
            [
                'key' => "general_address",
                'value' => "256, 1st AVE, Manchester 125 , Noth England",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_phone_1",
                'value' => "+011 222 3333",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_phone_2",
                'value' => "+044 555 666",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_email",
                'value' => "info@example.com",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "general_site_url",
                'value' => "www.example.com",
            ]
        );

        \App\Models\GeneralSetting::create(
            [
                'key' => "top_logo",
                'value' => 'catalog/logo.png',
            ]
        );
    }
}
