<!-- User Account Menu -->
<li class="dropdown user user-menu">
    <!-- Menu Toggle Button -->
    <a href="{{ route('users.edit', Auth::user()->id) }}">
        <!-- The user image in the navbar-->
        @if(isset(Auth::user()->avatar_id))
            <img src="{{ Storage::url((App\Models\UserAvatar::where('id', Auth::user()->avatar_id)->first())->path) }}" class="user-image" alt="User Image">
        @else
            <img src="@yield('user-avatar', 'https://www.gravatar.com/avatar/?d=mm')" class="user-image" alt="User Image">
        @endif
        <!-- hidden-xs hides the username on small devices so only the image appears. -->
        <span class="hidden-xs">{{ Auth::user()->name }}</span>
    </a>
</li>
