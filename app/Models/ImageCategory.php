<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageCategory extends Model
{
    public $timestamps = false;

    public $fillable = ['image_id', 'category_id'];

    public function image()
    {

        return $this->belongsTo('App\Models\ImageDB');

    }
    public function category()
    {

        return $this->belongsTo('App\Models\Category');

    }
}
