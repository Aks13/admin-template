# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Installation ###

1. `Clone` project
2. Run `composer install` in terminal
3. Rename `.env.example` to `.env` and set you default DB server settings.
4. Run `yarn install` or `npm install` in terminal
5. After set DB settings run `php artisan migrate:refresh --seed` in terminal
6. For login look for default user in `/database/seeds/UsersTableSeeder`
7. Only one time run `php artisan storage:link` in terminal
8. In `/public/storage` create directory `images`
9. In `/public/storage/images` create directory `catalog`
10. In `/public/storage/images` create directory `cache`
11. In `/public/storage/images/cache` create directory `catalog`
12. In `/public/storage/images/cache` create directory `tmp`
13. Create your engine at https://cse.google.com/cse/
14. Create API key at https://console.developers.google.com/ for Google CSE
15. Put your `GOOGLE_CSE_ID` and `GOOGLE_API_KEY` to `.env`
16. `ImageMagick` required! To install: `sudo apt-get install php-imagick`, `php -m | grep imagick`, `sudo service apache2 restart`
17. Set `QUEUE_CONNECTION=database` in `.env`
18. `php artisan queue:work 2>&1 &` to run job queue in background

### File manager demo ###

1. Login to `yoursitename.com/admin`
2. Go to `yoursitename.com/admin/generalSettings`
3. Click on `Logo` block

