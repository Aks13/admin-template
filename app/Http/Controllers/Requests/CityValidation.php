<?php

namespace App\Http\Requests;

use App\Models\City;
use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CityValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->can('cities')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $translateRule = [];

        foreach (config('translatable.locales') as $locale) {
            $translateRule['available_translation-' . $locale] = 'integer|min:1|max:1';
            $translateRule['name-' . $locale] = 'required_with:available_translation-'. $locale.'|max:255';
        }

        return array_merge([
            'country_id' => 'required|integer',
            'map_point' => 'regex:/^[0-9\-\.]+ [0-9\-\.]+$/',
            'zip_code' => 'regex:/^[0-9]+$/|nullable',
        ], $translateRule);
    }
}
