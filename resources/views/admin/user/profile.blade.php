@extends('admin.layouts.admin-lte')

@section('title', __('general.profile'))
@section('content-title', __('general.profile'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> {{ __('dashboard.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-user"></i> {{ __('general.edit') }} {{$user->name}}</li>
    </ol>
@endsection

@section('content')
    <script>
        $(document).ready(function(){
            $('#avatar').on('change', function(){
                var fd = new FormData();
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                fd.append('user_id', $('#user_id').val());
                fd.append('avatar', this.files[0]);
                $.ajax({
                    type: 'POST',
                    url: '{{route("uploadAvatar")}}',
                    data: fd,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function(response){
                        console.log('Response:', response);
                        $('#avatar-id').val(response.avatar.id);
                        $('#user-avatar').attr('src', response.avatar.url);
                        $('#avatar-name').val(response.avatar.name);
                        $('#avatar-path').val(response.avatar.path);

                        var btn = $('#user-avatar-remove');
                        btn.val(response.avatar.id);
                        if(btn.hasClass('hidden')) {
                            btn.removeClass('hidden');
                        }
                    },
                    error: function(response) {

                    }
                });
            });

            $('#user-avatar-remove').on('click', function() {
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    type: 'POST',
                    url: '{{route("removeAvatar")}}',
                    data: {'avatar_id': $('#user-avatar-remove').val()},
                    dataType: "json",
                    success: function(response) {
                        console.log('Response:', response);
                        $('#avatar-id').val('');
                        $('#user-avatar').attr('src', response.url);
                        $('#avatar-name').val('');
                        $('#avatar-path').val('');

                        var btn = $('#user-avatar-remove');
                        btn.val('');
                        if(!btn.hasClass('hidden')) {
                            btn.addClass('hidden');
                        }
                    }
                });
            });

        });
    </script>


    <div class="box">
        <div class="box-body">
            {{ Form::model($user, [
                'route' => array('profile.save'),
                'method' => 'PUT',
                'id' => 'edit_form'
            ]) }}

            <div class="col-xl-4">
                @if(isset($user->avatar->path))
                    <img src="{{ Storage::url($user->avatar->path) }}" id="user-avatar" alt="user-avatar" style="width: 10%">
                @else
                    <img src="{{ Storage::url('public/users/avatars/user.png') }}" id="user-avatar" alt="user-avatar" style="width: 10%">
                @endif
            </div>

            <label for="file">Choose file to upload</label>
            <input type="file" name="avatar" class="avatar" id="avatar" />

            @if(is_null($user->avatar_id))
                <button class="hidden btn btn-flat" id="user-avatar-remove" value="{{$user->avatar_id or ''}}">
                    <i class="fa fa-remove"></i>
                </button>
            @else
                <button class="btn btn-flat" id="user-avatar-remove" value="{{$user->avatar_id or ''}}">
                    <i class="fa fa-remove"></i>
                </button>
            @endif

            <input type="hidden" name="user_id" id="user_id" value="{{ $user->id }}">
            <input type="hidden" name="avatar_id" id="avatar-id" value="@if(old('avatar_id')){{old('avatar_id')}}@else{{$user->avatar_id or ''}}@endif">
            <input type="hidden" name="avatar_name" id="avatar-name" value="">
            <input type="hidden" name="avatar_path" id="avatar-path" value="">


            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                {{ Form::label('name', __('general.name')) }}
                {{ Form::text('name', null, array('class' => 'form-control')) }}
                @if ($errors->has('name'))
                    <span class="help-block">{{ $errors->first('name') }}</span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                {{ Form::label('email', __('general.email')) }}
                {{ Form::email('email', null, array('class' => 'form-control')) }}
                @if ($errors->has('email'))
                    <span class="help-block">{{ $errors->first('email') }}</span>
                @endif
            </div>

            <div class="alert fa-border alert-dismissible">

            <h4><i class="icon fa fa-info"></i> {{ __('general.password_unchanged') }}</h4>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                {{ Form::label('password', __('general.password')) }}<br>
                {{ Form::password('password', array('class' => 'form-control')) }}
                @if ($errors->has('password'))
                    <span class="help-block">{{ $errors->first('password') }}</span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                {{ Form::label('password', __('general.password_confirm')) }}<br>
                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
                @if ($errors->has('password_confirmation'))
                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                @endif
            </div>
            </div>
            {{ Form::submit(__('general.save'), array('class' => 'btn btn-primary')) }}

            {{ Form::close() }}
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(function() {
            $('#edit_form').submit(function(){
                $('#edit_form :password').each(function(){
                    if($(this).val() == '') {
                        $(this).attr("disabled", "disabled")
                    }
                });
            })
        });
    </script>
@endsection
