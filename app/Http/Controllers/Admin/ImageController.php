<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ImageThumbnail;
use App\Models\Category;
use App\Models\Image;
use App\Models\ImageCategory;
use App\Models\ImageDB;
use App\Models\ImageTool;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

/**
 * Are you idiot or what? Why did you extend AdminControlers from Controller?
 * Really?
 * You can look to another controllers and you can see whole
 * controler in admin groups extends from AdminController
 */
class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('hasPermission:admin');
        parent::__construct();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = ImageDB::whereNotNull('id')->paginate($this->paginate);
        return view('admin.images.index', [
            'images'=>$images,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $image = new ImageDB();
        $categories_global = Category::whereIn('id',Category::GLOBAL_CATEGORIES)->pluck('title','title');
        return view('admin.images.create', [
            'image'=>$image,
            'categories_global'=>$categories_global,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $files = $request->file('images');
        $categories = Category::whereIn('id',$data['category'])->with('parent')->get();

        foreach ($files as $image)
        {
            $name = $image->getClientOriginalName();
            $path = $image->store('public/images/catalog/');

            $file_info = pathinfo(storage_path('app/'.$path));

            $thumbnail = $path; //TODO make resize

            $imagedb = ImageDB::create([
                'title' => $name,
                'path' => $path,
                'thumbnail' => $thumbnail,
                'created_by' => $data['created_by'],
            ]);
            $type = strtolower($file_info['extension']);
            if ($type == 'gif')
            {
                $imagedb->type = 'gif';
            }
            $imagedb->save();

            ImageThumbnail::dispatch($imagedb);

            foreach ($categories as $category)
            {
                $image_category = ImageCategory::create([
                    'image_id' => $imagedb->id,
                    'category_id' => $category->id,
                ]);
                $image_category->save();
            }
        }
        return redirect()
            ->route('images.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.create_success') );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return redirect()
            ->route('images.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', 'SHOW'.$id );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return redirect()
            ->route('images.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', 'EDIT'.$id );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return redirect()
            ->route('images.index')
            ->with( 'message.level', 'success' )
            ->with( 'message.content', 'UPDATE'.$id );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageDB $image)
    {
//        dd($image);
        Storage::delete($image->path);

        $image->delete();
        return redirect()
            ->back()
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $data = $request->all();
        $images = ImageDB::whereIn('id',json_decode($data['ids']))->get();
        foreach ($images as $image)
        {
            Storage::delete($image->path);
            $image->delete();
        }

        return redirect()
            ->back()
            ->with( 'message.level', 'success' )
            ->with( 'message.content', __('general.delete_success') );
    }

    public function category($id)
    {
        $imgIds = ImageCategory::where('category_id',$id)->pluck('image_id');
//        dd($imgIds);
        $images = ImageDB::whereIn('id',$imgIds)->paginate(10);
        $category = Category::where('id',$id)->first();
        return view('admin.images.index', [
            'images'=>$images,
            'category'=>$category,
        ]);
    }
}
