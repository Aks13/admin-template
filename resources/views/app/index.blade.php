@extends('layouts.client')

@section('title', __('general.category'))
@section('content-title', __('general.category'))

@section('content')
    <div class="container-fluid">
        <div class="loader_wrapper">
            <div class="loader_wrapper_inner">
                <div class="loader">
                    <div class="loader__bar"></div>
                    <div class="loader__bar"></div>
                    <div class="loader__bar"></div>
                    <div class="loader__bar"></div>
                    <div class="loader__bar"></div>
                    <div class="loader__ball"></div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-12 pt-3 pb-3">

                <form method="GET">
                    <div class="filter-wrapper">
                        <select name="type" onchange="this.form.submit()" class="nice-init">
                            <option data-display="Type">Type</option>
                            <option value="gif" @if($data['type'] == "gif") selected @endif>Gif</option>
                            <option value="pic" @if($data['type'] == "pic") selected @endif>Pic</option>
                        </select>

                        <select name="sort" onchange="this.form.submit()" class="nice-init">
                            <option data-display="Sort" @if($data['sort'] == "Sort") selected @endif>Sort</option>
                            <option value="title+asc" @if($data['sort'] == "title+asc") selected @endif>By name (A-Z)</option>
                            <option value="title+desc" @if($data['sort'] == "title+desc") selected @endif>By name (Z-a)</option>
                            <option value="created_at+desc" @if($data['sort'] == "created_at+desc") selected @endif>By date (newest first)</option>
                            <option value="created_at+asc" @if($data['sort'] == "created_at+asc") selected @endif>By date (oldest first)</option>
                        </select>
                    </div>
                </form>
            </div>

            <div class="col-12 pt-4 pb-4 border-top-bottom">

                <div class="grid">


                    @php
                        $heights = [200, 300, 400, 250, 350, 220];
                    @endphp

                    @foreach($images as $image)
                        <div class="grid-item">
                            <a href="/{{ $image->src() }}" data-fancybox="gallery-fancy" class="item-link-wrapper">
                                <picture>
                                    <img data-src="/{{ $image->thumbnail() }}" alt="{{ $image->title }}" title="{{ $image->title }}" class="lazy_load" height="{{ $heights[array_rand($heights)] }}">
                                </picture>
                            </a>
                            <a href="/instaporn/download?image_id={{ $image->id }}" class="icon-download btn btn-danger btn-sm">
                                <i class="fa fa-cloud-download"></i>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="images-pagination">
                    {{$images->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection
