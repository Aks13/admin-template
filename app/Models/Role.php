<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 21.11.2018
 * Time: 15:41
 */

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use \Spatie\Permission\Models\Role as SpateRole;

class Role extends SpateRole
{
    use Notifiable;

    protected $fillable = [
        'name', 'guard_name',
    ];
}
