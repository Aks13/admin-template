$(window).on('load', function () {
    $('.loader_wrapper').fadeOut();
});

$(document).ready(function(){
    $('.navbar-toggler').on('click', function () {
        if($('.categories-list').hasClass('custom-visible')){
            $('.categories-list').removeClass('custom-visible')
        }else{
            $('.categories-list').addClass('custom-visible')
        }
    });

    $('.categories-list .super_span').click(function (e) {

        if($(this).closest('.category_wrapper_item').find('ul.inner_ul').hasClass('hidden_default')){
            $(this).closest('.category_wrapper_item').find('ul.inner_ul').removeClass('hidden_default');
        }else{
            $(this).closest('.category_wrapper_item').find('ul.inner_ul').addClass('hidden_default');
        }

    });

    $('.menu-item').hover(function () {
        if ($(document).width() > 768) {
            if($(this).find('.custom-item-elemetns').hasClass('hide')){
                $(this).find('.custom-item-elemetns').removeClass('hide');
            }else{
                $(this).find('.custom-item-elemetns').addClass('hide');
            }
        }
    });

    $('.menu-item').mouseleave(function () {
        if ($(document).width() > 768) {
            $(this).find('.custom-item-elemetns').addClass('hide');
        }
    });

    $('.navbar-toggler').click(function () {
        $(".categories-list").toggleClass( "invisible", 2000 );
        $(".overlay").toggleClass( "hiden-overlay", 2000 );
    });

    $('.categories-list .close_sidebar').click(function (e) {
        e.preventDefault();

        $(".categories-list").removeClass( "custom-visible" );
        $(".categories-list").addClass( "invisible" );
        $(".overlay").addClass( "hiden-overlay" );
    });

    $('.dropdown-toggle').click(function(e) {
         if ($(document).width() > 768) {
            e.preventDefault();
            var url = $(this).attr('href');
         if (url !== '#') {
            window.location.href = url;
            }
         }
    });

    //$.lazyLoadXT()
    $("img.lazy_load").on("scroll",function () {
        if ($(this).attr('src') !== $(this).attr('data-src') && $(document).scrollTop() + $(this).height() > $(this).offset().top && $(document).scrollTop() - $(this).offset().top < $(this).height())
        {
            $(this).lazyLoadXT();
        }
    }).on('load',function(){
        $('.grid').isotope({
            itemSelector: '.grid-item'
        });
    });

});
