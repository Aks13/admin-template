@extends('admin.layouts.admin-lte')

@section('title', __('general.cities'))
@section('content-title', __('general.cities'))

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ __('general.cities_list') }}</h3>
                    <div class="pull-right">
                        <a href="{{ route('cities.create') }}" class="btn btn-sm btn-success add_new_product">
                            {{ __('general.new') }}
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-hover table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>{{ __('general.country') }}</th>
                            <th>{{ __('general.city') }}</th>
                            <th>{{ __('general.zip_code') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($cities as $city)
                            <tr>
                                @if(isset($city->country->name))
                                    <td>{{ $city->country->name }}</td>
                                @else <td></td>
                                @endif
                                <td>{{ $city->name }}</td>
                                <td>{{ $city->zip_code }}</td>
                                <td class="td-action">
                                    <a class="btn btn-success btn-sm pull-left" href="{{ route('cities.edit', $city) }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <form onsubmit="if(confirm('{{ __('general.deleting') }}')){ return true }else{ return false }" action="{{ route('cities.destroy', $city) }}" method="post">
                                        <input type="hidden" name="_method" value="delete" />
                                        {{csrf_field()}}
                                        <button type="submit" class="btn-sm remove_item btn btn-danger pull-left"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td><h2>{{ __('general.empty_data') }}</h2></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                    <nav aria-label="page navigation">
                        <ul class="pagination">
                            {{$cities->links()}}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endsection

