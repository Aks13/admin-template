@extends('admin.layouts.admin-lte')

@section('title', __('general.filemanager'))
@section('content-title', __('general.filemanager'))

@section('content')
    <div class="box">
        <div class="box-body">
            <div class="form-group">
                <label class="col-sm-12 control-label">Test</label>
                <div class="col-sm-12">
                    <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                        <img src="" alt="" title="" data-placeholder="test" />
                    </a>
                    <input type="hidden" name="image" value="" id="input-image" />
                </div>
            </div>
        </div>
    </div>
@endsection

