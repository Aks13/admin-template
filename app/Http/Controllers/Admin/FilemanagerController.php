<?php

namespace App\Http\Controllers\Admin;

use App\Models\ImageTool;
use Illuminate\Http\Request;
use View;

class FilemanagerController extends AdminController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('hasPermission:fileManager');
        parent::__construct();
    }


    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request){
        return view('admin.filemanager.index');
    }

    public function show(Request $request)
    {
        $DIR_IMAGE = storage_path().'/app/public/images/';

        if ($request->has('filter_name')) {
            $filter_name = rtrim(str_replace(array('*', '/', '\\'), '', $request->input('filter_name')), '/');
        } else {
            $filter_name = '';
        }


        // Make sure we have the correct directory
        if ($request->has('directory')) {
            $directory = rtrim($DIR_IMAGE . 'catalog/' . str_replace('*', '', urldecode(urldecode($request->input('directory')))), '/');
        } else {
            $directory = $DIR_IMAGE . 'catalog';
        }

        if ($request->has('page')) {
            $page = $request->input('page');
        } else {
            $page = 1;
        }

        $directories = array();
        $files = array();

        $data['images'] = array();

        $image_tool_model = new ImageTool();

        // Get directories
        $directories = glob($directory . '/' . $filter_name . '*', GLOB_ONLYDIR);

        if (!$directories) {
            $directories = array();
        }

        // Get files
        $files = glob($directory . '/' . $filter_name . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE);

        if (!$files) {
            $files = array();
        }

        // Merge directories and files
        $images = array_merge($directories, $files);

        // Get total number of files and directories
        $image_total = count($images);

        // Split the array based on current page number and max number of items per page of 10
        $images = array_splice($images, ($page - 1) * 16, 16);

        foreach ($images as $image) {
            $name = str_split(basename($image), 14);

            $file_items = [];

            if (is_dir($image)) {

                $file_items['directory'] = urlencode(mb_substr($image, mb_strlen($DIR_IMAGE . 'catalog/')));

                if ($request->has('target')) {
                    $file_items['target'] = $request->input('target');
                }

                if ($request->has('thumb')) {
                    $file_items['thumb'] = $request->input('thumb');
                }

                $data['images'][] = array(
                    'thumb' => '',
                    'name'  => implode(' ', $name),
                    'type'  => 'directory',
                    'path'  => mb_substr($image, mb_strlen($DIR_IMAGE)),
                    'href'  => route('show-filemanager', $file_items)
                );
            } elseif (is_file($image)) {
                $data['images'][] = array(
                    'thumb' => $image_tool_model->resize(mb_substr($image, mb_strlen($DIR_IMAGE)), 100, 100),
                    'name'  => implode(' ', $name),
                    'type'  => 'image',
                    'path'  => mb_substr($image, mb_strlen($DIR_IMAGE)),
                    'href'  => $DIR_IMAGE . mb_substr($image, mb_strlen($DIR_IMAGE))
                );
            }
        }

        if ($request->has('directory')) {
            $data['directory'] = urlencode($request->input('directory'));
        } else {
            $data['directory'] = '';
        }

        if ($request->has('filter_name')) {
            $data['filter_name'] = $request->input('filter_name');
        } else {
            $data['filter_name'] = '';
        }

        // Return the target ID for the file manager to set the value
        if ($request->has('target')) {
            $data['target'] = $request->input('target');
        } else {
            $data['target'] = '';
        }

        // Return the thumbnail for the file manager to show a thumbnail
        if ($request->has('thumb')) {
            $data['thumb'] = $request->input('thumb');
        } else {
            $data['thumb'] = '';
        }

        // Parent
        $parent_items = [];

        if ($request->has('directory')) {

            $pos = strrpos(urldecode($request->input('directory')), '/');

            if ($pos) {
                $parent_items['directory'] = urlencode(substr($request->input('directory'), 0, $pos));
            }
        }

        if ($request->has('target')) {
            $parent_items['target'] = $request->input('target');
        }

        if ($request->has('thumb')) {
            $parent_items['thumb'] = $request->input('thumb');
        }

        $data['parent'] = route('show-filemanager', $parent_items);

        // Refresh
        $refresh_items = [];

        if ($request->has('directory')) {
            $refresh_items['directory'] = urlencode($request->input('directory'));
        }

        if ($request->has('target')) {
            $refresh_items['target'] = $request->input('target');
        }

        if ($request->has('thumb')) {
            $refresh_items['thumb'] = $request->input('thumb');
        }

        $data['refresh'] = route('show-filemanager', $refresh_items);

        //paginate
        $paginate_items = [];

        if ($request->has('directory')) {
            $paginate_items['directory'] = urlencode(html_entity_decode($request->input('directory'), ENT_QUOTES, 'UTF-8'));
        }

        if ($request->has('filter_name')) {
            $paginate_items['filter_name'] = urlencode(html_entity_decode($request->input('filter_name'), ENT_QUOTES, 'UTF-8'));
        }

        if ($request->has('target')) {
            $paginate_items['target'] = $request->input('target');
        }

        if ($request->has('thumb')) {
            $paginate_items['thumb'] = $request->input('thumb');
        }
        $paginate_items['page'] = '{page}';

        $pagination = $this->paginate($image_total, $page, 16, route('show-filemanager', $paginate_items));
        $data['pagination'] = $pagination;

        $view = \Illuminate\Support\Facades\View::make('admin.filemanager.partials.filemanagers', [ 'data' => $data ]);
        $content = $view->render();

        return $content;
    }

    public function folder(Request $request){

        $DIR_IMAGE = storage_path().'/app/public/images/';

         $json = array();

        // Make sure we have the correct directory
        if ($request->has('directory')) {
            $directory = rtrim($DIR_IMAGE . 'catalog/' . str_replace('*', '', urldecode(urldecode($request->input('directory')))), '/');
        } else {
            $directory = $DIR_IMAGE . 'catalog';
        }
        // Check its a directory
        if (!is_dir($directory)) {
            $json['error'] = __('general.filemanager_not_directory');
        }

        if ($request->server->get('REQUEST_METHOD') == 'POST') {
            // Sanitize the folder name
            $folder = basename(html_entity_decode($request->input('folder'), ENT_QUOTES, 'UTF-8'));

            // Validate the filename length
            if ((mb_strlen($folder) < 3) || (mb_strlen($folder) > 128)) {
                $json['error'] = __('general.filemanager_more_symbols');
            }

            // Check if directory already exists or not
            if (is_dir($directory . '/' . $folder)) {
                $json['error'] = __('general.filemanager_exist');
            }
        }

        if (!isset($json['error'])) {
            mkdir($directory . '/' . $folder, 0777);
            chmod($directory . '/' . $folder, 0777);

            @touch($directory . '/' . $folder . '/' . 'index.html');

            $json['success'] = __('general.filemanager_folder_done');
        }

        return response()->json($json);
    }

    public function delete(Request $request){
        $json = array();

        $DIR_IMAGE = storage_path().'/app/public/images/';

        if($request->has('path')) {
            $paths = $request->input('path');
        } else {
            $paths = array();
        }

        // Loop through each path to run validations
        foreach ($paths as $path) {
            // Check path exsists
            if ($path == $DIR_IMAGE . 'catalog') {
                $json['error'] = 'Низзя !';

                break;
            }
        }

        if (!$json) {
            // Loop through each path
            foreach ($paths as $path) {
                $path = rtrim($DIR_IMAGE . $path, '/');

                // If path is just a file delete it
                if (is_file($path)) {
                    unlink($path);

                    // If path is a directory beging deleting each file and sub folder
                } elseif (is_dir($path)) {
                    $files = array();

                    // Make path into an array
                    $path = array($path);

                    // While the path array is still populated keep looping through
                    while (count($path) != 0) {
                        $next = array_shift($path);

                        foreach (glob($next) as $file) {
                            // If directory add to path array
                            if (is_dir($file)) {
                                $path[] = $file . '/*';
                            }

                            // Add the file to the files to be deleted array
                            $files[] = $file;
                        }
                    }

                    // Reverse sort the file array
                    rsort($files);

                    foreach ($files as $file) {
                        // If file just delete
                        if (is_file($file)) {
                            unlink($file);

                            // If directory use the remove directory function
                        } elseif (is_dir($file)) {
                            rmdir($file);
                        }
                    }
                }
            }

            $json['success'] = __('general.filemanager_delete_done');
        }

        return response()->json($json);
    }

    public function upload(Request $request) {

        $DIR_IMAGE = storage_path().'/app/public/images/';

        $json = array();

        // Make sure we have the correct directory
        if ($request->has('directory')) {
            $directory = rtrim($DIR_IMAGE . 'catalog/' . str_replace('*', '', urldecode(urldecode($request->input('directory')))), '/');
        } else {
            $directory = $DIR_IMAGE . 'catalog';
        }
        // Check its a directory
        if (!is_dir($directory)) {
            $json['error'] = __('general.filemanager_upload_in_files');
        }

        if (!$json) {

            $files = $request->file('file');

            foreach ($files as $key => $file){
                if (is_file($file->getPathname())) {
                    // Sanitize the filename
                    $filename = basename(html_entity_decode($file->getClientOriginalName(), ENT_QUOTES, 'UTF-8'));

                    // Validate the filename length
                    if ((mb_strlen($filename) < 3) || (mb_strlen($filename) > 255)) {
                        $json['error'] = __('general.filemanager_file_name');
                    }

                    // Allowed file extension types
                    $allowed = array(
                        'jpg',
                        'jpeg',
                        'gif',
                        'png'
                    );

                    if (!in_array(mb_strtolower(mb_substr(strrchr($filename, '.'), 1)), $allowed)) {
                        $json['error'] = __('general.filemanager_extention_error');
                    }

                    // Allowed file mime types
                    $allowed = array(
                        'image/jpeg',
                        'image/pjpeg',
                        'image/png',
                        'image/x-png',
                        'image/gif'
                    );

                    if (!in_array($file->getMimeType(), $allowed)) {
                        $json['error'] = __('general.filemanager_extention_error');
                    }
                } else {
                    $json['error'] = __('general.filemanager_upload_error');
                }

                if (!$json) {
                    move_uploaded_file($file->getPathname(), $directory . '/' . $filename);
                }
            }
        }

        if (!$json) {
            $json['success'] = __('general.filemanager_upload_done');
        }

        return response()->json($json);
    }

    public function paginate($total, $page, $limit, $url){
        $num_links = 3;
        $text_first = '|&lt;';
        $text_last = '&gt;|';
        $text_next = '&gt;';
        $text_prev = '&lt;';


        if ($page < 1) {
            $page = 1;
        }

        if (!(int)$limit) {
            $limit = 1;
        }

        $num_pages = ceil($total / $limit);

        $url = str_replace('%7Bpage%7D', '{page}', $url);

        $output = '<ul class="pagination">';

        if ($page > 1) {
            $output .= '<li><a href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $url) . '">' . $text_first . '</a></li>';

            if ($page - 1 === 1) {
                $output .= '<li><a href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $url) . '">' . $text_prev . '</a></li>';
            } else {
                $output .= '<li><a href="' . str_replace('{page}', $page - 1, $url) . '">' . $text_prev . '</a></li>';
            }
        }

        if ($num_pages > 1) {
            if ($num_pages <= $num_links) {
                $start = 1;
                $end = $num_pages;
            } else {
                $start = $page - floor($num_links / 2);
                $end = $page + floor($num_links / 2);

                if ($start < 1) {
                    $end += abs($start) + 1;
                    $start = 1;
                }

                if ($end > $num_pages) {
                    $start -= ($end - $num_pages);
                    $end = $num_pages;
                }
            }

            for ($i = $start; $i <= $end; $i++) {
                if ($page == $i) {
                    $output .= '<li class="active"><span>' . $i . '</span></li>';
                } else {
                    if ($i === 1) {
                        $output .= '<li><a href="' . str_replace(array('&amp;page={page}', '?page={page}', '&page={page}'), '', $url) . '">' . $i . '</a></li>';
                    } else {
                        $output .= '<li><a href="' . str_replace('{page}', $i, $url) . '">' . $i . '</a></li>';
                    }
                }
            }
        }

        if ($page < $num_pages) {
            $output .= '<li><a href="' . str_replace('{page}', $page + 1, $url) . '">' . $text_next . '</a></li>';
            $output .= '<li><a href="' . str_replace('{page}', $num_pages, $url) . '">' . $text_last . '</a></li>';
        }

        $output .= '</ul>';

        if ($num_pages > 1) {
            return $output;
        } else {
            return '';
        }
    }
}
