<div class="row">
    <div class="col-sm-6">
        <div class="form-group {{ $errors->has('symbols') ? 'has-error' : ''}}">
            {!! Form::label('symbols', __('general.symbols')) !!}
            {!! Form::text('symbols',$country->symbols ?? null,['class' => 'form-control','placeholder' => __('general.symbols'),'size' => 255,]) !!}
            {!! $errors->first('symbols', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group {{ $errors->has('map_point') ? 'has-error' : ''}}">
            {!! Form::label('map_point', __('general.map_point')) !!}
            {!! Form::text('map_point',$country->map_point ?? '0 0',['class' => 'form-control','placeholder' => __('general.map_point'),'size' => 255]) !!}
            {!! $errors->first('map_point', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>

@include('admin.country.partials.translations')