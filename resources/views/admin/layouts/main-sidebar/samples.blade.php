<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        @section('sidebar-menu')
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Samples</li>
                <li><a href="#"><i class="fa fa-book text-red"></i> <span>One</span></a></li>
                <li>
                    <a href="#">
                        <i class="fa fa-th text-yellow"></i> <span>Two</span>
                        <span class="pull-right-container">
            <small class="label pull-right bg-green">new</small>
          </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-calendar text-aqua"></i> <span>Three</span>
                        <span class="pull-right-container">
            <small class="label pull-right bg-red">3</small>
            <small class="label pull-right bg-blue">17</small>
          </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-envelope"></i> <span>Four</span>
                        <span class="pull-right-container">
            <small class="label pull-right bg-yellow">12</small>
            <small class="label pull-right bg-green">16</small>
            <small class="label pull-right bg-red">5</small>
          </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Five</span>
                        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Five One</a></li>
                        <li class="active"><a href="#"><i class="fa fa-circle-o"></i> Five Two</a></li>
                    </ul>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-share"></i> <span>Six</span>
                        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
                    </a>

                <li><a href="#"><i class="fa fa-circle-o"></i> Six One</a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-circle-o"></i> Six Two
                        <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Six Two One</a></li>
                        <li class="treeview active">
                            <a href="#"><i class="fa fa-circle-o"></i> Six Two Two
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="#"><i class="fa fa-circle-o"></i> Six Two Two One</a></li>
                                <li class="active"><a href="#"><i class="fa fa-circle-o"></i> Six Two Two
                                        Two</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Six Three</a></li>
            </ul>
            </li>
            </ul>
    @show
    <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
