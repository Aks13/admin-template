<?php

namespace App\Http\Controllers\Requests;

use App\Models\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryValidation extends FormRequest
{
    //
    public function rules(Request $request)
    {
        $rules = [
            'title' => 'required|string|max:255|unique:categories',
            'meta' => 'nullable',
            'description' => 'nullable',
            'text' => 'nullable',
            'parent_id' => 'required|integer|exists:categories,id',
        ];
        if($this->method() == "PATCH")
        {
            $rules['title'] .= ',id,'.$request->get('id');
        }
        return $rules;
    }
}
