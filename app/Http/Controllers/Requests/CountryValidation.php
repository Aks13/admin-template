<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CountryValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->can('countries')) {
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $translateRule = [];

        foreach (config('translatable.locales') as $locale) {
            $translateRule['available_translation-' . $locale] = 'integer|min:1|max:1';
            $translateRule['name-' . $locale] = 'required_with:available_translation-'. $locale.'|max:255';
        }

        switch ( $this->method() ) {
            case 'GET':
            case 'DELETE': {
                return [];
            }
            case 'POST': {
                return array_merge([
                    'symbols' => 'required|unique:countries,symbols|max:2',
                    'map_point' => 'regex:/^[0-9\-\.]+ [0-9\-\.]+$/',
                ], $translateRule);
            }
            case 'PUT':
            case 'PATCH': {
                return array_merge([
                    'symbols' => 'required|max:2|unique:countries,symbols,' . $this->country->id,
                    'map_point' => 'required|regex:/^[0-9\-\.]+ [0-9\-\.]+$/',
                ], $translateRule);
            }
            default:
                return [];
        }
    }
}
