<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      @yield('content-title', 'Title')
      <small>@yield('content-subtitle')</small>
    </h1>
    @section('breadcrumbs')
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Title</li>
    </ol>
    @show
  </section>

  <!-- Main content -->
  <section class="content">

    @if(session()->has('message.level'))
      <div class="alert alert-{{ session('message.level') }} alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="icon fa fa-{{ session('message.level') }}"></i> {{ session('message.content') }}
      </div>
    @endif
    <section class="content">
      @yield('content')
    </section>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
