<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 12.03.2019
 * Time: 10:07
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if(null == Auth::user()){
            return redirect()->route('login');
        }
        if (!Auth::user()->hasPermissionTo($permission)) {
            abort('401');
        }
        return $next($request);
    }
}
