<?php

namespace App\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
//    use Translatable;

    const GLOBAL_CATEGORIES = [ 1, 2 ];

    protected $fillable = [
        'parent_id',
        'title',
        'meta',
        'description',
        'text',
        'created_by',
        'modified_by',
    ];

    public static function withoutParents()
    {
        return Category::whereIn('id',Category::GLOBAL_CATEGORIES)->with('children')->get();
    }

    public function parent()
    {
        return $this->hasOne('App\Models\Category','id','parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category', 'parent_id','id');
    }

    public function images()
    {
//        return $this->hasManyThrough('App\Models\Category','App\Models\ImageCategory','image_id','id','id','category_id');
        return $this->hasManyThrough('App\Models\ImageDB', 'App\Models\ImageCategory','category_id','id', 'id', 'image_id');
    }


    public static function navigation()
    {
        $html = '<ul class="navbar-nav mr-auto">';

        $categories = Category::whereNull('parent_id')->with('children')->get();
        foreach ($categories  as $category) {
            if(isset($category->children[0]))
            {
                $html .= '<li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="'.route('image-category', $category).'" id="dropdown-'.$category->title.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$category->title.'</a>
                                <div class="dropdown-menu" aria-labelledby="dropdown-'.$category->title.'">';
                $html .= '<a class="dropdown-item" href="'.route('image-category', $category).'">'.$category->title.'</a>';
                foreach ($category->children as $child) {
                    $html .= '<a class="dropdown-item" href="'.route('image-category', $child).'">'.$child->title.'</a>';
                }
                $html .= '</div></li>';
            }
            else
            {
                $html .= '
                    <li class="nav-item">
                        <a class="nav-link" href="'.route('image-category', $category).'">'.$category->title.'</a>
                    </li>';
            }
        }
        $html .= '</ul>';
        return $html;

    }

    public static function adminnavigation()
    {
        $html = '<div class="col">';
        $html .= '<a type="button" class="btn btn-info" href="'.route('images.index').'">All</a>';

        $categories = Category::whereNull('parent_id')->with('children')->get();
        foreach ($categories  as $category) {
            if(isset($category->children[0]))
            {
                $html .= '<div class="btn-group">
                            <a type="button" class="btn btn-info" href="'.route('admin.image.by-category', $category).'">'.$category->title.'</a>
                            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown">
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">';
                foreach ($category->children as $child) {
                    $html .= '<li><a class="dropdown-item" href="'.route('admin.image.by-category', $child).'">'.$child->title.'</a></li>';
                }
                $html .= '</ul>
                      </div>';
            }
            else
            {
                $html .= '<a type="button" class="btn btn-info" href="'.route('admin.image.by-category', $category).'">'.$category->title.'</a>';
            }
        }
        $html .= '</div>';
        return $html;

    }

    public function url_noslug()
    {
        $url = htmlspecialchars($this->title);

        if(isset($this->parent->title))
        {
            $url = $this->parent->url_noslug() . '/' . $url;
        }

        return $url;
    }

    public function url_slug()
    {
        $url = Str::slug($this->title);

        if(isset($this->parent->title))
        {
            $url = $this->parent->url_slug() . '/' . $url;
        }

        return $url;
    }

    public static function getByUrl($url)
    {
        $urls = explode('/', $url);
//        dd($urls);
        $category = new Category();
        if (count($urls)>1)
        {
            $category_1 = null;
            foreach ($urls as $url_i)
            {
                $title_like = str_replace('-', ' ', $url_i);
                $category = Category::where('title','like',$title_like)->first();

                if($category_1)
                {
                    $category = Category::where('title','like',$title_like)
                                        ->where('parent_id',$category_1->id)
                                        ->first();
                    dd($category);
                }
                $category_1 = $category;
            }
        }
        else
        {
            $title_like = str_replace('-', ' ', $urls[0]);
            $category = Category::where('title','like',$title_like)->first();
        }
        return $category;
    }

    public static function getByUrl_noslug($url)
    {
        $urls = explode('/', $url);
//        dd($urls);
        $category = new Category();
        if (count($urls)>1)
        {
            $category_1 = null;
            foreach ($urls as $url_i)
            {
                $title_like = htmlspecialchars_decode($url_i);
                $category = Category::where('title','like',$title_like)->first();

                if($category_1)
                {
                    $category = Category::where('title','like',$title_like)
                                        ->where('parent_id',$category_1->id)
                                        ->first();
//                    dd($category);
                }
                $category_1 = $category;
            }
        }
        else
        {
            $title_like = htmlspecialchars_decode($urls[0]);
            $category = Category::where('title','like',$title_like)->first();
        }
        return $category;
    }
}
